package com.ld.lucenex.plugin;

import com.ld.lucenex.base.BaseConfig;
import com.ld.lucenex.config.LuceneXConfig;

/**
 *spring bean
 */
public class SpringLucenePlugin {

    LuceneXConfig config;
    SpringLucenePlugin(LuceneXConfig config){
        this.config = config;
    }

    public void init(){
        BaseConfig.configLuceneX(this.config);
    }
}
